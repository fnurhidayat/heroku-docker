FROM node:12.16-alpine
MAINTAINER FikriRNurhidayat@gmail.com

WORKDIR app
COPY . .

ARG env
ENV NODE_ENV=$env

RUN npm install
CMD npm start
