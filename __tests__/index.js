const request = require('supertest')
const server = require('../src/server')

test('Should return correct HTML render', async done => {
  const response = await request(server).get('/')
  expect(response.status).toEqual(200)
  expect(response.text).toMatchSnapshot()
  done()
})
