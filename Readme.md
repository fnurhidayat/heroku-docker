# Heroku Docker

This is a simple repository to demonstrate how you can deploy to heroku using Docker image.

## Setup

You'll only need to add these two variables inside the Gitlab CI/CD Environmetn Variables:

```
HEROKU_API_KEY=yourherokuapikey
HEROKU_APP_NAME=yourherokuappname
```

And you're done!

## How to run this repository?

It's simple, you'll only need `Node.JS v12.16`. And then clone this repo:

```bash
npm install
npm start
```

Default port is `8000`. To run the test you can just simply run:

```bash
npm test
```
